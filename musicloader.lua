music = {
	thread = love.thread.newThread("musicloader_thread.lua"),
	loaderreq  = love.thread.getChannel("musicloader_request"),
	loaderresp = love.thread.getChannel("musicloader_response"),
	toload = {},
	loaded = {},
	list = {},
	list_fast = {},
	pitch = 1,
}

function music:init()
	self.thread:start()
end

function music:load(musicfile) -- can take a single file string or an array of file strings
	if type(musicfile) == "table" then
		for i,v in ipairs(musicfile) do
			self:preload(v)
		end
	else
		self:preload(musicfile)
	end
--	print ("sending table: ", dump(self.toload))
	self.loaderreq:push(self.toload)
end

function music:preload(musicfile)
	if self.loaded[musicfile] == nil then
		self.loaded[musicfile] = false
		table.insert(self.toload, musicfile)
	end
end

function music:fetch(name)
--	print("fetch called for: ", name)
	while true do
		local msg = self.loaderresp:demand()
		local nm, source = next(msg)
--		print ("Got resources ", nm, " ", source)
		if (nm and source) then
			self:onLoad(nm, source)
			if nm == name then break end
		end
	end
end

function music:play(name)
	if name and soundenabled then
		if self.loaded[name] == false then
			music:fetch(name)
		end
		if self.loaded[name] then
			playsound(self.loaded[name])
		end
	end
end

function music:playIndex(index, isfast)
	local name = isfast and self.list_fast[index] or self.list[index]
	self:play(name)
end

function music:stop(name)
	if name and self.loaded[name] then
		love.audio.stop(self.loaded[name])
	end
end

function music:stopIndex(index, isfast)
	local name = isfast and self.list_fast[index] or self.list[index]
	self:stop(name)
end

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function music:update()
	for i,v in ipairs(self.toload) do
		if not self.loaded[v] then
			self:fetch(v)
		end
	end
	local err = self.thread:getError()
	if err then print(err) end
end

function music:onLoad(name, source)
--	print (name,".onLoad()")
	self.loaded[name] = source
	source:setLooping(true)
	source:setPitch(self.pitch)
end


music:load{
	"overworld",
	"overworld-fast",
	"underground",
	"underground-fast",
	"castle",
	"castle-fast",
	"underwater",
	"underwater-fast",
	"starmusic",
	"starmusic-fast",
	"princessmusic",
}

-- the original/default music needs to be put in the correct lists
for i,v in ipairs(music.toload) do
	if v:match("fast") then
		table.insert(music.list_fast, v)
	elseif not v:match("princessmusic") then
		table.insert(music.list, v)
	end
end

music:init()

