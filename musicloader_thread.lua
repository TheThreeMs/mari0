require("love.filesystem")
require("love.sound")
require("love.audio")
require("love.timer")

love.filesystem.setIdentity("mari0")

local musicpath = "sounds/%s.ogg"

local musiclist = {}
local musictoload = {} -- waiting to be loaded into memory

local loaderreq  = love.thread.getChannel("musicloader_request")
local loaderresp = love.thread.getChannel("musicloader_response")

local function getmusiclist()
	local requested = loaderreq:demand()
	if requested then
		for i,v in ipairs(requested) do
			if not musiclist[v] then
--				print("Loading: ", v)
				musiclist[v] = true
				table.insert(musictoload, v)
			end
		end
	end
end

local function getfilename(name)
	local filename = name:match("%.[mo][pg][3g]$") and name or musicpath:format(name) -- mp3 or ogg
	if love.filesystem.getInfo(filename, "file") then
		return filename
	else
		print(string.format("thread can't load \"%s\": not a file!", filename))
	end
end

local function loadmusic()
	while #musictoload > 0 do
		local name = table.remove(musictoload, 1)
		local filename = getfilename(name)
		if filename then
			local source = love.audio.newSource(love.sound.newDecoder(filename, 512 * 1024), "static")
			print("thread loaded music", name)
			loaderresp:push({ [name]=source })
		end
	end
end

while true do
	getmusiclist()
	loadmusic()
end
